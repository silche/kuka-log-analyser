import math

import plotly.graph_objects as go
from plotly.subplots import make_subplots

import dash_app
from common import *
from themes import arrival_dark as style

logo_symbol = "\uD83D\uDFB7"

# Titles
title_text = "distribution of differences between " \
             "current and expected states over time".title()
sub_title = f"<br><sub>{chr(0x20) * 13}© <span style='font-family:Courier New'>2021</sub></span>"
legend_title = "joints︱absolute maximums".upper()
chart_titles = [f"joint {q + 1}".upper() for q in range(joints_amount)]
xaxes_title = 'time'.upper()
yaxes_title = "deviation of state from target".upper()

hover_template = "<b>Value:</b> %{y:.12f}<br><b>Time: </b> %{x}"
extra = "<extra>%%{fullData.%s}</extra>"

app_layout_height = '100vh'

view_modes = {
    'multiple': False,  # displays charts on one chart sheet
    'grid': False,  # displays side by side
    'stack': False  # as a scrollable stack
}


def create_plots():
    global app_layout_height
    rows, columns = 1, 1  # Total number of rows and columns
    vertical_spacing = None

    if view_modes['grid'] and joints_amount:
        columns = math.ceil(joints_amount / 2)
        rows = math.ceil(joints_amount / columns)
        vertical_spacing = 0.2
    elif view_modes['stack']:
        rows, columns = joints_amount if joints_amount else 1, 1
        layout_height = 715  # in pixels
        app_layout_height = layout_height * rows
        vertical_spacing = 23 * rows / app_layout_height

    return make_subplots(subplot_titles=chart_titles,
                         vertical_spacing=vertical_spacing,
                         rows=rows, cols=columns)


def set_main_title(color_scheme):
    logo_color = style.logo_black if color_scheme == 'light' else style.logo_white
    logo = f"<span style='font-size:19.5;color:{logo_color}'>{logo_symbol}</span>"
    return logo + chr(0x20) * 5 + title_text + sub_title


def set_figure_layout(figure, color_scheme):
    if color_scheme and color_scheme in style.plt_io.templates:
        figure.layout.template = color_scheme
    figure.update_layout(title_text=set_main_title(color_scheme),
                         legend_title_text=legend_title)
    if view_modes['stack']:
        figure.update_layout(height=app_layout_height, showlegend=False)


def arrange(joint, row, column, columns):
    """
    Distribution of charts over the display area

    :param joint: Joint's number
    :param row: Row number of the current chart
    :param column: Column number of the current chart
    :return: Row and column numbers for the next chart
    :param columns: Total number of columns
    """
    if view_modes['stack']:
        row += 1
    elif view_modes['grid'] and columns > 1:
        column += 1
        if joint == math.ceil(joints_amount / 2 - 1):
            column = 1
            row += 1

    return row, column


def set_axes(figure, times, columns):
    max_number_of_ticks = 12
    number_of_ticks = int(max_number_of_ticks / columns)
    trimmed_times = [time.strftime('%H:%M:%S.%f')[:-3] for time in times]
    tick_values, tick_text = ([] for _ in range(2))

    for tick in range(1, number_of_ticks):
        index = tick * math.ceil(len(times) / number_of_ticks)
        tick_values.append(times[index])
        tick_text.append(trimmed_times[index])

    figure.update_xaxes(title=xaxes_title, tickmode='array',
                        tickvals=tick_values, ticktext=tick_text)
    figure.update_yaxes(title_text=yaxes_title)


def set_annotations(figure):
    font_size = 14.5 if view_modes['stack'] else 13
    figure.update_annotations(font={'size': font_size, 'family': "Courier New"})


def set_charts_visibility(figure):
    trace_selector = dict(legendgroup=chart_titles[0]) if view_modes['multiple'] else None
    figure.update_traces(visible=True, selector=trace_selector)


def add_markers(figure, times, columns):
    text_position = 'middle right'
    marker_text = "Max"
    shadow_color = figure.layout.template.layout.plot_bgcolor
    shadow_template = f'<span style="text-shadow: ' \
                      f'{shadow_color} -.5px 0px 0px, ' \
                      f'{shadow_color} .5px -.5px 0px, ' \
                      f'{shadow_color} -.5px .5px 0px, ' \
                      f'{shadow_color} .5px .5px 0px;">'

    row, column = 1, 1
    for joint in range(joints_amount):
        max_value = differences_of_states[joint]['max']
        if view_modes['multiple']:
            marker_text = f"{chart_titles[joint]} max".title()
            text_position = 'top center' if max_value > 0 else 'bottom center'

        marker_text_template = shadow_template + marker_text + "</span>"

        figure.add_trace(go.Scatter(x=[times[differences_of_states[joint]['index_max_diff']]],
                                    y=[max_value], mode='markers+text',
                                    marker=dict(color=style.ticks_marker_color, size=7,
                                                line=dict(width=0.5, color=shadow_color)),
                                    name=f"Max = {differences_of_states[joint]['max']:.8f}",
                                    legendgroup=chart_titles[joint], visible='legendonly',
                                    texttemplate=marker_text_template,
                                    text=marker_text, textposition=text_position,
                                    textfont=dict(color=style.ticks_marker_color, size=13.5),
                                    hovertemplate=hover_template + extra % 'text'),
                         row=row, col=column)
        row, column = arrange(joint, row, column, columns)


def build_charts(figure, trace_mode):
    """
    Create a 2D line plot of the data in Y (differences of states)
    versus the corresponding values in X (time)
    """
    row, column = 1, 1
    columns = figure._get_subplot_rows_columns()[1].stop - 1
    # Only time is taken for display
    times = [time.time() for time in timestamps]

    for joint in range(joints_amount):
        figure.add_trace(go.Scatter(x=times, y=differences_of_states[joint]['difference'],
                                    name=chart_titles[joint], legendgroup=chart_titles[joint],
                                    line_width=1.20, visible='legendonly',
                                    mode=trace_mode, marker_size=1.30,
                                    text=chart_titles[joint].capitalize(),
                                    hovertemplate=hover_template + extra % 'text'),
                         row=row, col=column)
        row, column = arrange(joint, row, column, columns)

    add_markers(figure, times, columns)
    set_axes(figure, times, columns)
    set_annotations(figure)
    set_charts_visibility(figure)


def view_by_default(view_mode):
    print(f"The view mode '{view_mode}' was not found") \
        if view_mode else print("\nView mode was not specified.")

    print("You can select one of the available viewing modes:")
    for view_mode in view_modes:
        print(f"'{view_mode}'", end=' ')

    print("\n\nBuilding charts by default...")
    view_modes['multiple'] = True


def set_view_mode(view_mode):
    # Using "in" is the fastest way to check the existence of a key in the view_modes dictionary
    if view_mode in view_modes:
        view_modes[view_mode] = True
    else:
        view_by_default(view_mode)


def display_charts(view_mode, color_scheme, trace_mode):
    """
    Displaying charts based on read and calculated data from the log file as a 2D line plots

    :param view_mode:
        mode of displaying and distributing charts on a chart sheet
        or an entire web page: 'multiple', 'grid' or 'stack'
    :param color_scheme: light color scheme of presentation
    :param trace_mode: 'markers' represents each data point of the chart line as a marker point
    """
    set_view_mode(view_mode)

    # make_subplots() is similar to go.Figure() in our implementation
    figure = make_subplots() if view_modes['multiple'] else create_plots()
    set_figure_layout(figure, color_scheme)
    build_charts(figure, trace_mode)

    view_mode = [v_mode for v_mode, value in view_modes.items() if value is True]
    print(f"Displaying charts in {view_mode} view mode...\n")

    dash_app.run(figure, app_layout_height)
