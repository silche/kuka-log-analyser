# KUKA log analyser

GUI web application for visualising and analysing the movement of a KUKA robot based on differences between states and targets of each joint.

Third-party libraries/modules used: `numpy`, `plotly`, `dash`. Their installation or activation of the virtual environment of the project may be required. For the latter, use the command: `source ./venv/binc/activate`

| Input | Output |
| :---: | :----: |
| KUKA robot debug log file | GUI web page on localhost |

Example of a line from a log file containing the required joint state data:
> 2020-01-31 03:16:30.232135;    DEBUG;TKRD; message=task manager report; task_name=follow trajectory; state.ts=1580458590231000; state.q1= -0.787143; state.q2= -1.56905; state.q3= 1.56556; state.q4= 0; state.q5= 0.0174533; state.q6= 0; target.q1= -0.786612; target.q2= -1.57008; target.q3= 1.5672; target.q4= -2.02302e-06; target.q5= 0.013914; target.q6= 8.16513e-05

<div align="center">
<img alt="Grid view mode" src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/grid_view_mode.png"
    title="Grid view mode" width="70%">
<p><em>Grid view</em></p>
</div>

#### Application options

The program has several options that can be used by specifying certain arguments before starting the application.

- ***Log file***<br>
Option `-f` or `--file` with a parameter of a specific path to the log file<br>
To provide the log file to the program without manually specifying the path, the log file can be placed in the default directory: `kuka_robot_log`

- ***Charts presentation options***<br>
Option `-v` or `--view` with a view mode parameter: `multiple` (set by default), `grid` or `stack`<br>
    - `Grid`. Charts are displayed side by side on the same screen
    - `Multiple`. All charts are displayed on one chart sheet
    - `Stack`. Charts are displayed sequentially one after the other as a scrollable stack
<p>

- **_Light color scheme_**<br>
Option `-l` or `--light` without additional parameters<br>
Representation in light colors. The default is a dark color scheme

- **_Markers_**<br>
Option `-m` or `--markers` without additional parameters<br>
Representing each data point of the chart line as a marker point<br>
<p>

#### Screenshots
<details><summary>Default view after starting the app</summary>
<div align="center">
<img alt="Multiple view 1" src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/multiple_view/multiple_1.png"
    title="Multiple view 1" width="45%">
<img alt="Multiple view 2" src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/multiple_view/multiple_2.png"
    title="Multiple view 2" width="45%">
<p><em>Default view after starting the app</em></p>
</div>
</details>

<details><summary>Grid view examples</summary>
<div align="center">
<img alt="Grid view. Light color scheme" src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/light_color_scheme/grid_view/grid_view_line.png"
    title="Grid view. Light color scheme" width="70%">
<p><em>Grid view. Light color scheme</em></p>
</div>

<div align="center">
<img alt="Grid view. Markers mode" src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/grid_view_mode_markers.png"
    title="Grid view. Markers mode" width="70%">
<p><em>Grid view. Markers mode</em></p>
</div>
</details>

<details><summary>Multiple view examples</summary>
<div align="center">
<img alt="Multiple view 3" src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/multiple_view/multiple_3.png"
    title="Multiple view 3" width="40%">
<img alt="Multiple view mode" src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/multiple_view/multiple_5.png"
    title="Multiple view mode" width="40%">
<p><i>Multiple view</i></p>
</div>

<div align="center">
<img alt="Multiple view. Markers mode" src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/multiple_view/markers/multiple_markers_4.png"
    title="Multiple view. Markers mode" width="40%">
<img alt="Multiple view. Markers mode" src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/multiple_view/markers/multiple_markers_5.png"
    title="Multiple view. Markers mode" width="40%">
<p><i>Multiple view. Markers mode</i></p>
</div>
</details>

<details><summary>Charts from Stack view</summary>
<div style="text-align:center">
<p align="center">
<img alt="Joint 3 Chart"
    src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/light_color_scheme/joints_from_stack_view/joint_3_chart.png"
    title="Joint 3 Chart" width="40%">
<img alt="Joint 4 Chart"
    src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/light_color_scheme/joints_from_stack_view/anomalies/joint_4_chart_anom.png"
    title="Joint 4 Chart" width="40%">
<img alt="Joint 5 Chart"
    src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/light_color_scheme/joints_from_stack_view/anomalies/joint_5_chart_anom.png"
    title="Joint 5 Chart" width="40%">
<img alt="Joint 6 Chart"
    src="https://gitlab.com/silche/kuka-log-analyser/-/raw/media/screenshots/light_color_scheme/joints_from_stack_view/anomalies/joint_6_chart_anom_2.png"
    title="Joint 6 Chart" width="40%">
<br><em>Charts from Stack view</em>
</p>
</div>
</details>

[**Link to all screenshots**](https://gitlab.com/silche/kuka-log-analyser/-/tree/media/screenshots)
