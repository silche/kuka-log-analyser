import random
import webbrowser
from threading import Timer

import dash
import dash_core_components as dcc
import dash_html_components as html

host = '127.0.0.1'
port = 5000 + random.randint(0, 999)
url = f"http://{host}:{port}/"


def run(figure, app_layout_height):
    """
    Start the Dash application server and display it in a web browser tab

    :param figure:
        Built charts layout
    :param app_layout_height:
        App layout height. Usually the same as the figure layout height
    """
    app = dash.Dash(title="TKRD Test Task")

    app.layout = html.Div([html.Div(
        children=dcc.Graph(figure=figure, style={'height': 'inherit'}),
        style={'height': app_layout_height})])

    # Launching the browser in a separate thread to reduce the display time of the web app
    Timer(0.0, webbrowser.open_new_tab(url)).start()

    # Launch Dash app
    app.server.run(port=port, debug=False, use_reloader=False)
