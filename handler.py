import re
from datetime import datetime

import numpy as np

from common import *

time_pattern = \
    r'^([0-9\-]*\s[0-9\-:.+]*).*?(?:;\s\w*.q[0-9]=\s([0-9\-e.]*))'
state_pattern = r'state.q[0-9]=\s([0-9\-e.]*)'
target_pattern = r'target.q[0-9]=\s([0-9\-e.]*)'

states = [[] for _ in range(joints_amount)]
targets = [[] for _ in range(joints_amount)]


def parse(file):
    """
    Analysis of data by specific regular expressions.
    Saving the read data into the appropriate lists

    :param file: Opened for reading file
    """
    regex_pattern = re.compile(time_pattern)

    for log_line in file:
        data = regex_pattern.match(log_line)
        if not data:
            continue

        timestamps.append(datetime.strptime(data.group(1),
                                            "%Y-%m-%d %H:%M:%S.%f"))
        state = re.findall(state_pattern, log_line)
        target = re.findall(target_pattern, log_line)

        for q in range(joints_amount):
            states[q].append(float(state[q]))
            targets[q].append(float(target[q]))


def read_log(log_file):
    """
    Reading data from a file at the specified path

    :param log_file: Kuka robot log data file
    """
    with open(log_file, 'r') as file:
        parse(file)
    file.close()


def calculate_difference():
    """
    Calculating the difference between current and target states
    """
    for q in range(joints_amount):
        differences_of_states.append({'joint': q + 1, 'difference': []})
        for cell in range(len(timestamps)):
            differences_of_states[q]['difference'].append(
                float(states[q][cell]) -
                float(targets[q][cell]))


def maximum_difference():
    """
    Finding the absolute maximum value of difference from the calculated ones
    """
    for q in range(joints_amount):
        # Conversion to a specific array object
        array_of_differences = np.array(differences_of_states[q]['difference'])
        index_max_diff = abs(array_of_differences).argmax()
        differences_of_states[q]['index_max_diff'] = index_max_diff
        differences_of_states[q]['max'] = \
            differences_of_states[q]['difference'][index_max_diff]


def get_log_data(log_file):
    """
    Reading robot movement data from the specified log file

    :param log_file: Path to the kuka robot log data file
    """
    read_log(log_file)
    calculate_difference()
    maximum_difference()
