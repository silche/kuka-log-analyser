from themes.arrival_light import *

logo_white = 'white'
background_color = 'rgb(15, 16, 17)'  # almost black
plot_background_color = 'rgb(40, 41, 44)'  # = 'rgba(46, 47, 50, 0.88)'
grid_color = 'rgb(107, 107, 107)'  # dark grey
font_color = 'rgb(243, 243, 243)'  # slightly gray

# Custom "dark" template inherited from "light"
plt_io.templates["dark"] = plt_io.templates["light"]
plt_io.templates.default = "dark"

axes = dict(
    gridcolor=grid_color,
    linecolor=grid_color,
)

plt_io.templates["dark"].update(
    dict(
        layout=dict(
            font=dict(color=font_color),
            paper_bgcolor=background_color,
            plot_bgcolor=plot_background_color,
            xaxis=axes, yaxis=axes,
            legend=dict(
                bgcolor=plot_background_color,
                font_color=font_color,
                bordercolor=plot_background_color
            )
        )
    )
)
