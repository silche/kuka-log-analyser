import plotly.io as plt_io

# Colours
logo_black = 'rgb(15, 16, 17)'
plot_background_color = 'rgb(248, 248, 248)'
legend_background_color = 'rgb(236, 237, 237)'
grid_color = 'rgb(201, 202, 203)'  # light grey
font_color = 'rgb(35, 38, 44)'  # almost black
ticks_marker_color = '#DB6E4C'  # light brown

# Layout appearance settings
layout = dict(
    title=dict(font_size=18, x=.03, xanchor="left"),
    font=dict(color=font_color),
    plot_bgcolor=plot_background_color,
    margin=dict(l=80, r=50, b=100, t=100, pad=3),
    autosize=True,
    hovermode='closest',
    colorway=['#1F77B4', '#97c71f', '#deaf21',
              '#e111d1', '#1eae83', '#5b43b4']
)

# Initialize custom "light" template
plt_io.templates["light"] = {'layout': layout}

# Axes appearance settings
axes = dict(
    showgrid=True,
    showline=True,
    gridcolor=grid_color,
    linecolor=font_color,
    title_font_size=12.3,
    title_font_family="Courier New",
    showticklabels=True,
    ticks='inside',
    tickcolor=ticks_marker_color,
    tickwidth=2, ticklen=6,
)
plt_io.templates["light"]['layout']['xaxis'] = axes
plt_io.templates["light"]['layout']['yaxis'] = axes

# Legend appearance settings
plt_io.templates["light"]['layout']['legend'] = dict(
    title_font_size=13.5,
    title_font_family="Courier New",
    font_size=13,
    font_family="Courier New",
    font_color=font_color,
    bgcolor=legend_background_color,
    bordercolor=legend_background_color,
    borderwidth=11,
    itemsizing='trace',
    traceorder='grouped'
)
