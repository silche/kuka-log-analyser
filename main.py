# GUI web application for visualising and analysing the movement of a KUKA robot
# based on differences between states and targets of each joint.

import argparse
import sys

import handler
import visualizer
from common import *

default_log_file_path = "kuka_robot_log/kuka_robot.log"


def init_argparse() -> argparse.ArgumentParser:
    app_description = "GUI web app for analysing the movement of a KUKA robot" \
                      "based on differences between states and targets of joints."
    parser = argparse.ArgumentParser(usage="%(prog)s [-h] [-f FILE] [-v VIEW] [-l] [-m]",
                                     description=app_description)
    parser.add_argument('commands', action='count')
    parser.add_argument('-f', '--file', default=default_log_file_path, help="specific path to the log file")
    parser.add_argument('-v', '--view', help="view mode: 'multiple', 'grid' or 'stack'")
    parser.add_argument('-l', '--light', action='store_true',
                        help="representation in light colors, dark color scheme by default")
    parser.add_argument('-m', '--markers', action='store_true',
                        help="represents each data point of the chart line as a marker point")
    return parser


# Analysis of the joints of a KUKA robot
def analyse_log():
    parser = init_argparse()

    # Check availability of arguments
    try:
        sys.argv[1]
    except:
        parser.print_help()

    options = parser.parse_args()
    color_scheme = 'light' if options.light else ""
    trace_mode = 'markers' if options.markers else "lines"

    # Processing data from the specified file
    try:
        handler.get_log_data(options.file)
    except (FileNotFoundError, IsADirectoryError) as err:
        print(f"{parser.prog}: {options.file}: {err.strerror}", file=sys.stderr)

    print("Absolute maximum difference state-target")
    for q in range(joints_amount):
        print(f"\tJoint {q + 1}: "
              f"{differences_of_states[q]['max']}")

    visualizer.display_charts(options.view, color_scheme, trace_mode)


# Entry point
if __name__ == '__main__':
    analyse_log()
